## IM-DF Rate test
### Documentation and tools by Jannicke Pearkes - Stanford, Summer 2017

[Her copy of the documentation (Evernote logbook)](https://www.evernote.com/shard/s305/sh/0e76688f-f6ff-4edd-be2b-484c51ae941b/689f57c6699e85ea79449fe5df9bd4b0)

***

From the pub machine:

```
$ .  /atlas-home/1/jpearkes/setup_multi_df.sh
$ setup_daq FTK_DF_shelf1_test
```

In Run Control, and enable one of the DFs (for example here I used 3-05)
Make sure the IBL inputs channels are enabled and the correct boards are enabled in df\_multiboard_config.txt
Commit and reload in RC

From the tdaq computer:
Then run the following script after pressing config and before pressing start in RC, make sure to change the to DF you want to test:

```$ bash /atlas-home/0/tomoya/DataFormatter/script_IMTest_dev/USA15_ignoreDFXoff.sh DF-3-05``` - titled" BPignore.sh in this repo

Then press start in RC

To monitor average event rates for individual channels search in IMSMON by using:
```$ im_monitor -p FTK_DF_shelf1_test -D DF-3-05 -M 4 -F 2 -N 0 ```

To look at all channels on DF modify the input DF list in dump_im.py and parse_imsmon.py and then run:

```
$ python /atlas-home/1/jpearkes/dump_imsmon.py
$ python /atlas-home/1/jpearkes/parse_imsmon.py
```

### Documenation of a 2017 test...

**Purpose:**

Investigated whether the 45kHz IBL fw performance was related to backpressure from the DF or not. This allows us to isolate whether the slow rate is due the the IM or the interplay between the IM and DF

**Summary:**

Turned off backpressure and ran 1 module 50 word pseudo-data on board 3-05 only

Average rate per channel in kHz is as follows:

```
IBL  DF-3-05_F0_N0 Average rate:    45
SCT DF-3-05_F0_N1 Average rate:     0
PIX  DF-3-05_F1_N0 Average rate:     533
SCT DF-3-05_F1_N1 Average rate:      0
PIX  DF-3-05_F2_N0 Average rate:    533
SCT DF-3-05_F2_N1 Average rate:     634
PIX  DF-3-05_F3_N0 Average rate:    533
SCT DF-3-05_F3_N1 Average rate:     634
IBL  DF-3-05_F4_N0 Average rate:    0
SCT DF-3-05_F4_N1 Average rate:     0
PIX  DF-3-05_F5_N0 Average rate:     533
SCT DF-3-05_F5_N1 Average rate:      0
PIX  DF-3-05_F6_N0 Average rate:     533
SCT DF-3-05_F6_N1 Average rate:      634
PIX  DF-3-05_F7_N0 Average rate:     533
SCT DF-3-05_F7_N1 Average rate:      634
```

This means that the IBL channels (only 0 in this case, 8 is turned off for 3-05) are running slowly.
The results also show that the SCT modules are capable of running at 634 kHz and the PIX modules are capable of running at 533 kHz for this type of pseudodata.

Instructions to replicate:

```
Used release 02-00-05
DF Firmware = 98326
FPGA=0 is 0Xc0f3
FPGA=1 is 0Xc0f0
FPGA=2 is 0X40f0
FPGA=3 is 0X40f0
FPGA=4 is 0Xc0f3
FPGA=5 is 0Xc0f0
FPGA=6 is 0X40f0
FPGA=7 is 0X40f0
```