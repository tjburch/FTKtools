import sys
import glob
import subprocess

partition = "FTK_DF_shelf1_test" # name of partition
dfs = ["DF-1-06"]
#dfs= ["DF-1-03", "DF-1-04"         ,"DF-1-06", "DF-1-07", "DF-1-08" ,"DF-1-09","DF-1-10",]
# "DF-3-03", "DF-3-04","DF-3-05",                    "DF-3-08" ,"DF-3-09","DF-3-10",
# "DF-4-03", "DF-4-04","DF-4-05","DF-4-06","DF-4-07","DF-4-08" ,"DF-4-09","DF-4-10"]# DF to test
fpga = 0 # IM FPGA to monitor [0-7]
MAX_FPGA = 8 # Test FPGAs up to this one
channel  = 0 # Channel on FPGA to monitor [0-1]
MAX_CHANNEL = 2 # Test channels up to this on
num_cycles = 1 # Number of times to repeat test

for df in dfs:
    for fpga in range(MAX_FPGA):
        for channel in range(MAX_CHANNEL):
            # Make a directory for the output
            talk = "mkdir "+df+"_F"+str(fpga)+"_N"+str(channel)
            print(talk)
            subprocess.call(talk, shell=True)
            # Monitor num_cycles times
            for cycle in range(num_cycles): 
                file_name = (df+"_F"+str(fpga)+"_N"+str(channel)+"/"+
                  df+"_F"+str(fpga)+"_N"+str(channel)+"_C"+str(cycle))
                talk = ("im_monitor  -p "+partition+" -D "+df
                +" -M 4 -F "+str(fpga)+" -N "+str(channel)
                +" >> " +file_name)
                print(talk)
                subprocess.call(talk, shell=True)
