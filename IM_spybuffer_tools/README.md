## IMbufferCheck.py 

This script is designed to check consistency in IM spybuffers or Test Vectors.

At lab 4 you can run as:
python IMbufferCheck.py -f InputFile.dat [-o OutputName] [-r]

If no output name given, will publish as ./FILENAME_DATE_nWords.pdf
If -r flag passed, publishes histograms to a root file as well (same name)
Don't put any extensions on output file name

At p1, ROOT functionality does not exist, so do not pass [-r]

Currently, the script does the following things:

##### inSpys:
* Indicates if a channel is inactive
* Checks every b0f00000 is followed by an e0f000000
* Checks e0f00000 is not repeated - on events where buffer loops over, this is fine
* Gets number of data words from trailer, fills histogram for each channel
* Ensures that L1ID's increases monotonically
* Outputs number of "bad" events per channel (b0f before e0f, L1ID increase problem)
* Outputs number of "good" events, and total events

#### outSpys:
* Indicates if a channel is inactive
* Makes sure every b0f00000 is followed by an e0f000000
* Checks e0f00000 is not repeated - on events where buffer loops over, this is fine
* Gets number of data words from *counting*, fills histogram for each channel
* Ensures that L1ID's increases monotonically
* Outputs number of "bad" events per channel (b0f before e0f, L1ID increase problem)
* Outputs number of "good" events, and total events

#### Test Vectors:
* Makes sure every b0f00000 is followed by an e0f000000
* Gets number of data words from *counting*, fills histogram
* Ensures that L1ID's increases monotonically
* Outputs number of "bad" (b0f before e0f, L1ID increase problem)
* Outputs number of "good" events, and total events


Writes histograms of nWords for each channel to indicated .pdf (and root file if option passed)

If you need histograms for p1 spybuffer, move the it to an area with ROOT and run.

#### TODO:
 
* Refactor - very old code
	* Develop class at buffer level, event level
	* Make simple enough to import into IMbufferAnalytics.py
* Add counting of modules

## IMbufferAnalytics.py 

Similar script, reports useful quantities (nEvents and mean, median, max, standard deviation nWords/event), either per channel or summed over all channels

Creates histograms

Requires matplotlib and numpy (so run locally or on lxplus after moving buffers.

#### TODO:
* Most of this can be stripped out after IMbufferCheck becomes more modular and can be imported

## cabling_check.py 

Compares RODIDs reported by ID (actual) to that in our dictionary (intended). Flags any mismatches.

Can be used while running, or afterwards with a spybuffer asd input

#### TODO:
* Scrape DF names from IS or similar
* Run im_monitor internally, use as input text, and have a clean exit
* Add to solo run