###############################
# Compares RODs in Spy Buffer #
# to cabling dictionary       #
# tyler.james.burch@cern.ch   #
###############################
import json
import sys
import re


def is_testbed():
    import socket
    if 'tbed' in socket.gethostname():
        return True
    else:
        return False


def fix_dictionary(df_dict):
    # drop extra junk
    return_dict = {}
    for key in df_dict:
        new_lane = []
        for lane in df_dict[key]:
            if type(lane) is list:
                new_lane.append(lane)
        return_dict[key] = new_lane
    return return_dict


def parse_for_rods(spybuffer):
    spybuffer_dict = {}

    if type(spybuffer) is list:
        spybuffer = ''.join(spybuffer)

    for i, line in enumerate(spybuffer.splitlines()):
        if 'FTK_IM channel' in line:
            channel = line.split('is')[1].strip()
            rod_id = spybuffer.splitlines()[i + 1].split('is')[1].strip()
            if rod_id.startswith('0x00'):
                rod_id = rod_id.replace('0x00', '0x')
            spybuffer_dict[channel] = rod_id

    return spybuffer_dict


def get_is_object(spybuffer):
    df = ''
    for line in spybuffer:
        if is_testbed():
            try:
                df = re.search('DF\d\d', line).group(0)
                return df
            except AttributeError:
                continue
        else:
            # check for slice
            try:
                df = re.search('DF-\d-\d\d-slice', line).group(0)
                return df
            except AttributeError:
                continue
    if df == '' and not is_testbed():
        for line in spybuffer:
            try:
                df = re.search('DF-\d-\d\d', line).group(0)
                return df
            except AttributeError:
                continue

    return 0


def print_output(channel, this_rod_dict, this_rod_spy, df_name):
    # Handle all cases of output
    if this_rod_dict == this_rod_spy:
        return False
    else:
        print "Problem on channel %s" % (channel)
        print "     Expected ROD: %s" % (this_rod_dict)
        print "       Actual ROD: %s" % (this_rod_spy)
        return False


def add_special_cases(df_dict):
    df_dict['DF22'] = [['0x140103', '', '0', '', True],
                       ['0x000000', '', '1', '', True],
                       ['0x111750', '', '2', '', True],
                       ['0x000000', '', '3', '', True],
                       ['0x112521', '', '4', '', True],
                       ['0x220100', '', '5', '', True],
                       ['0x112559', '', '6', '', True],
                       ['0x220101', '', '7', '', True],
                       ['0x140093', '', '8', '', True],
                       ['0x000000', '', '9', '', True],
                       ['0x130109', '', '10', '', True],
                       ['0x220102', '', '11', '', True],
                       ['0x111710', '', '12', '', True],
                       ['0x000000', '', '13', '', True],
                       ['0x130108', '', '14', '', True],
                       ['0x220103', '', '15', '', True]]

    df_dict['DF20'] = [['0x140103', '', '0', '', True],
                       ['0x000000', '', '1', '', True],
                       ['0x111750', '', '2', '', True],
                       ['0x000000', '', '3', '', True],
                       ['0x112521', '', '4', '', True],
                       ['0x220100', '', '5', '', True],
                       ['0x112559', '', '6', '', True],
                       ['0x220101', '', '7', '', True],
                       ['0x140093', '', '8', '', True],
                       ['0x000000', '', '9', '', True],
                       ['0x130109', '', '10', '', True],
                       ['0x220102', '', '11', '', True],
                       ['0x111710', '', '12', '', True],
                       ['0x000000', '', '13', '', True],
                       ['0x130108', '', '14', '', True],
                       ['0x220103', '', '15', '', True]]

    df_dict['DF-2-04-slice'] = [['0x140103', '', '0', '', True],
                                ['0x220100', '', '1', '', True],
                                ['0x111750', '', '2', '', True],
                                ['0x000000', '', '3', '', True],
                                ['0x112521', '', '4', '', True],
                                ['0x000000', '', '5', '', True],
                                ['0x112559', '', '6', '', True],
                                ['0x220101', '', '7', '', True],
                                ['0x140093', '', '8', '', True],
                                ['0x000000', '', '9', '', True],
                                ['0x130149', '', '10', '', True],
                                ['0x220102', '', '11', '', True],
                                ['0x111710', '', '12', '', True],
                                ['0x000000', '', '13', '', True],
                                ['0x130146', '', '14', '', True],
                                ['0x220103', '', '15', '', True]]

    df_dict['DF-2-09-slice'] = [['0x140110', '', '0', '', True],
                                ['0x210105', '', '1', '', True],
                                ['0x111714', '', '2', '', True],
                                ['0x000000', '', '3', '', True],
                                ['0x130114', '', '4', '', True],
                                ['0x210104', '', '5', '', True],
                                ['0x111754', '', '6', '', True],
                                ['0x210106', '', '7', '', True],
                                ['0x000000', '', '8', '', True],
                                ['0x000000', '', '9', '', True],
                                ['0x130115', '', '10', '', True],
                                ['0x210107', '', '11', '', True],
                                ['0x112445', '', '12', '', True],
                                ['0x000000', '', '13', '', True],
                                ['0x112405', '', '14', '', True],
                                ['0x000000', '', '15', '', True]]

    return df_dict


def get_text(spybuffer):
    with open(spybuffer) as f:
        content = f.readlines()
    return content


def check_dfs(spybuffer_dictionary, df_dict):
    # Effectively the "main" - compares spybuffer rods to df dict

    errors = False

    for channel in spybuffer_dictionary:

        # Get ROD as seen by spybuffer
        this_rod_spy = spybuffer_dictionary[channel]

        # If nonzero, Get ROD as seen by df_dictionary
        this_rod_dict = '0x000000'
        for lane in df_dict[df_name]:
            if lane[2] == channel:
                this_rod_dict = lane[0]

        # Zero case - not cabled
        if this_rod_spy[-6:] == '000000' and this_rod_dict[-6:] != '000000':
            print "Channel %s is not connected" % (channel)
            errors = True
            continue
        if (this_rod_dict[-6:] == '000000' and this_rod_spy[-6:] != '000000'):
            print "Channel %s is not in our dictionary" % (channel)
            print "     Connected ROD: %s" % this_rod_spy
            errors = True
            continue

        if print_output(channel, this_rod_dict, this_rod_spy, df_name):
            errors = True

    return errors


if __name__ in '__main__':

    try:
        content = get_text(sys.argv[1])
    except IndexError:
        print "Pass a text file with im_monitor output as argument!"
        print "Exiting..."
        sys.exit(1)

    # Get DF Dictionary as labeled
    if is_testbed():
        df_dict = json.load(open('/tbed/user/tburch/public/FTKtools/dfDict/dfDict.json'))
    else:
        df_dict = json.load(open('/det/ftk/tools/DF_cabling/dfDict.json'))

    df_dict = fix_dictionary(df_dict)

    # Take care of special cabling cases
    df_dict = add_special_cases(df_dict)

    spy_df = get_is_object(content)

    if spy_df == 0:
        print "Unable to return DF name"
        sys.exit()
    else:
        dfs_to_check = [get_is_object(content)]

    spybuffer_dictionary = {}

    # Format df_name to match dfDict
    # cast as int to get rid of leading 0
    if not is_testbed():
        if 'slice' not in spy_df:
            shelf = int(spy_df.split('-')[1])
            slot = int(spy_df.split('-')[2])
            df_name = str(shelf) + '-' + str(slot)
        else:
            df_name = spy_df
    else:
        df_name = spy_df

    # Make sure it's in the dictionary
    if df_name not in df_dict:
        print 'DF %s does not exist in spreadsheet' % df_name
        sys.exit(1)
        errors = True

    # add rods from current DF to dictionary
    spybuffer_dictionary.update(parse_for_rods(content))

    errors = check_dfs(spybuffer_dictionary, df_dict)
    if not errors:
        print "Cabling okay!"
        sys.exit(0)
    else:
        print "Cabling is incorrect."
        sys.exit(1)
