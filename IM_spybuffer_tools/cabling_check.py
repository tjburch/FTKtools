###############################
# Compares RODs in Spy Buffer #
# to cabling dictionary       #
# tyler.james.burch@cern.ch   #
###############################
import json
import sys
import os
import argparse
import subprocess
import pdb


def is_testbed():
    import socket
    if 'tbed' in socket.gethostname():
        return True
    else:
        return False


def parse_for_rods(spybuffer):
    spybuffer_dict = {}

    if type(spybuffer) is list:
        spybuffer = ''.join(spybuffer)

    for i, line in enumerate(spybuffer.splitlines()):
        if 'FTK_IM channel' in line:
            channel = line.split('is')[1].strip()
            rod_id = spybuffer.splitlines()[i + 1].split('is')[1].strip()
            spybuffer_dict[channel] = rod_id

    return spybuffer_dict


def get_is_object(spybuffer):
    for line in spybuffer:
        if 'IS object' in line:
            return line.split('IS object', 1)[1].split()[0]
    return 0


def print_output(spy_ch, dict_ch, this_rod_dict, this_rod_spy, df_name):
    # Handle all cases of output
    if dict_ch == -1:
        print "DF: %s Channel: %s is not found in cabling dictionary" % (df_name, spy_ch)
        return True
    elif int(dict_ch) != int(spy_ch) and this_rod_dict == this_rod_spy:
        print "DF %s has mismatched ROD ID %s: " % (df_name, this_rod_spy)
        print "   Cabling dict ch: %s RODID: %s" % (dict_ch, this_rod_dict)
        print "    Spy Channel ch: %s " % (spy_ch)
        return True
    elif int(dict_ch) == int(spy_ch):
        return False
    else:
        return True


def add_special_cases(df_dict):
    df_dict['DF22'] = [['0x140103', '', '0', '', True],
                       ['0x000000', '', '1', '', True],
                       ['0x111750', '', '2', '', True],
                       ['0x000000', '', '3', '', True],
                       ['0x112521', '', '4', '', True],
                       ['0x220100', '', '5', '', True],
                       ['0x112559', '', '6', '', True],
                       ['0x220101', '', '7', '', True],
                       ['0x140093', '', '8', '', True],
                       ['0x000000', '', '9', '', True],
                       ['0x130109', '', '10', '', True],
                       ['0x220102', '', '11', '', True],
                       ['0x111710', '', '12', '', True],
                       ['0x000000', '', '13', '', True],
                       ['0x130108', '', '14', '', True],
                       ['0x220103', '', '15', '', True]]

    df_dict['DF-2-04-slice'] = [['0x140110', '', '0', '', True],
                                ['0x210105', '', '1', '', True],
                                ['0x111714', '', '2', '', True],
                                ['0x000000', '', '3', '', True],
                                ['0x130114', '', '4', '', True],
                                ['0x210104', '', '5', '', True],
                                ['0x111754', '', '6', '', True],
                                ['0x210106', '', '7', '', True],
                                ['0x140120', '', '8', '', True],
                                ['0x000000', '', '9', '', True],
                                ['0x130115', '', '10', '', True],
                                ['0x210107', '', '11', '', True],
                                ['0x112445', '', '12', '', True],
                                ['0x000000', '', '13', '', True],
                                ['0x112405', '', '14', '', True],
                                ['0x000000', '', '15', '', True]]

    return df_dict


def get_active_dfs(args):
    # This command uses is_ls to get all objects in IS then grep/awks it's way to DF names
    pipe = subprocess.Popen('is_ls -p %s | grep "DF\.FTK" | awk \'{print $1}\' | grep -v ":" ' % args.partition,
                            shell=True, stdout=subprocess.PIPE)
    return pipe.stdout.read().splitlines()


def get_text(args):
    # Gets text appropriate to input type

    # Input Spybuffer - reads all content
    if args.spybuffer:
        with open(args.spybuffer) as f:
            content = f.readlines()

    # List of DFs - run im_monitor for each
    elif args.objectList:
        content = ''
        for df in args.objectList:
            for fpga in range(0, 8):
                for channel in range(0, 1):
                    pipe = subprocess.Popen('im_monitor -p %s -D %s -M 4 -F %s -N %s' %
                                            (args.partition, df, fpga, channel), shell=True, stdout=subprocess.PIPE)
                    content += pipe.stdout.read()
    else:
        content = ''

    return content


def check_dfs(spybuffer_dictionary, df_dict):
    # Effectively the "main" - compares spybuffer rods to df dict

    errors = False
    # Run over all channels
    for channel in spybuffer_dictionary:
        this_rod_spy = spybuffer_dictionary[channel]

        dict_channel = -1

        # Zero Case
        if this_rod_spy[-6:] == '000000' or this_rod_spy == '0x0' or this_rod_spy == '0':
            if df_dict[df_name][int(channel)][0] != '0x000000':
                errors = True

        # Nonzero Case
        else:
            for lane in df_dict[df_name]:
                this_rod_dict = lane[0]

                if this_rod_dict[-6:] in this_rod_spy:
                    dict_channel = lane[2]

            if print_output(channel, dict_channel, this_rod_dict, this_rod_spy, df_name):
                errors = True

    return errors


if __name__ in '__main__':

    parser = argparse.ArgumentParser(description='Check actual cabling vs dictionary')
    parser.add_argument('-s', dest='spybuffer', required=False, help='Input Spybuffer File')
    parser.add_argument('-l', dest='objectList', nargs='+', required=False,
                        help='Input List of DFs (separated by spaces)')
    parser.add_argument('-p', dest='partition', required=False,
                        default='FTK_SliceA', help='Partition Name')
    args = parser.parse_args()

    # If running an object list, check a partition is setup
    # If not forcing commandline partition, accept environment partition
    if args.objectList:
        if not os.environ.get('FTK_RELEASE'):
            print "Please setup a FTK release!"
            sys.exit()
        if not args.partition:
            args.partition = os.environ.get('TDAQ_PARTITION')

    # TODO: Get DF names from Readout Modules
    # [tburch@pc-atlas-pub-03 ~]$ is_ls -p FTK_SliceA | grep 'Histogramming.FTK_DF'

    # Get DF Dictionary as labeled
    if is_testbed():
        df_dict = json.load(open('/tbed/user/tburch/public/FTKtools/dfDict/dfDict.json'))
    else:
        df_dict = json.load(open('/det/ftk/tools/DF_cabling/dfDict.json'))

    # Take care of special cabling cases
    df_dict = add_special_cases(df_dict)

    # Parse Spybuffer or run im_monitor
    content = get_text(args)

    if args.spybuffer:
        spy_df = get_is_object(content)
        if spy_df == 0:
            print "Unable to return DF name"
            sys.exit()
        else:
            dfs_to_check = [get_is_object(content)]
    else:
        dfs_to_check = get_active_dfs(args)  # args.objectList

    spybuffer_dictionary = {}
    for df in dfs_to_check:

        # Format df_name to match dfDict
        # cast as int to get rid of leading 0
        if args.spybuffer:
            if not is_testbed():
                shelf = int(df.split('-')[1])
                slot = int(df.split('-')[2])
                df_name = str(shelf) + '-' + str(slot)
            else:
                df_name = df.split('.')[2]
        else:
            df_name = df

        # Make sure it's in the dictionary
        if df_name not in df_dict:
            print 'DF %s does not exist in spreadsheet' % df_name
            continue

        # add rods from current DF to dictionary
        spybuffer_dictionary.update(parse_for_rods(content))

    errors = check_dfs(spybuffer_dictionary, df_dict)

    if not errors:
        print "Cabling matches spreadsheet"
    else:
        print "error detected"
