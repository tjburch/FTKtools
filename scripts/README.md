# Scripts

SaveAllSpyISEMon\_IMDF\_v0.sh - Standard spybuffer and monitoring info dump script

get\_fw\_versions.sh - prints fw versions

### Directories:

* freeze - Scripts to enable/disable freeze for tests
* rateTest - written by Jannicke (Stanford, Summer 2017). Scripts to run and parse rate tests
* xoffcheck - Xoff enabling/disabling scripts
* slink - basic scripts for SLINK tests