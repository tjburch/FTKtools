# e.g. FTK_DF, IM-DF-AUX-AMB-SSB-FLIC
partition=${1}
# e.g. 3 (DF22 at lab4)
shelf=${2}
# e.g. 9 (DF22 at lab4)
slot=${3}
# e.g. DF22, DF-3-08 etc
df=${4}
# e.g. FTK-RCD-DF-Slice2, FTK-RCD-DF-MultiBoards, FTK-RCD-DF-Shelf3-4, FTK-RCD-DF
MonitoringOutput=${5:-FTK-RCD-DF}

#echo "aaa "${partition}" "${df}" "${shelf}" "${slot}

date=`date +%Y%m%d%H%M`
file="ISIMAllSpy_"$df"_"$date".dat"

if [ $# -lt 4 ];  then
    echo ""
    echo "usage : bash SaveAllSpyISEMon_IMDF.sh [partition] [shelf] [slot] [dfID] [RCD]"
    echo "example of Lab 4 by DF22"
    echo ">bash SaveAllSpyISEMon_IMDF.sh [IM-DF-AUX-AMB] [3] [3] [DF22] [RCD]"
    echo "ERROR  4 arguments required"
    echo ""
    exit 1
fi



echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "H Save all spy regs on IM                              H"
echo "H save df mon, df mon, im inspy, im outspy (~2 min)    H"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"


echo "============ Description =============" > ${file}
echo "Please write description of reason for save the IM spy data by single line.";
echo "e.g. Data flow stop with unknown reason etc";
echo -n "Description >"
read des;

echo $des >> ${file}
echo "" >> ${file}
echo "" >> ${file}

#### Write the Firmware version
echo "============ Firmware Versions =============" >> ${file}
test_df -D ${df} -M 21 >> ${file}
for i in {0..7}; do test_im_bt -M 2 -D ${df} -F ${i} >> ${file}; done
echo "" >> ${file}
echo "" >> ${file}

#### Run DF Monitoring
echo "============ df mon =============" >> ${file}
echo -n "DF mon start (~3 sec)"
test_df -p ${partition} -D  ${df} -M 2 >> ${file}
echo "      done"
echo "" >> ${file}
echo "" >> ${file}


if echo "$HOSTNAME" | grep -q "tbed";
then
	TESTBED=true
	source /tbed/ftk/tools/checkerrors-DF.sh ${df} >> ${file}
else
	TESTBED=false
	source /det/ftk/tools/checkerrors-DF.sh ${df} >> ${file}
fi

echo "" >> ${file}
echo "" >> ${file}
echo "      done"


#### Write IM buffers
echo -n "IM mon start (~10 sec)"
echo "" >> ${file}
echo "" >> ${file}
echo "============ im mon =============">> ${file}
im_monitor  -p ${partition} -D  ${df} -M 4 -F 0 -N 0 >> ${file} #0
im_monitor  -p ${partition} -D  ${df} -M 4 -F 0 -N 1 >> ${file} #1
im_monitor  -p ${partition} -D  ${df} -M 4 -F 1 -N 0 >> ${file} #2
im_monitor  -p ${partition} -D  ${df} -M 4 -F 1 -N 1 >> ${file} #3
im_monitor  -p ${partition} -D  ${df} -M 4 -F 2 -N 0 >> ${file} #4
im_monitor  -p ${partition} -D  ${df} -M 4 -F 2 -N 1 >> ${file} #5
im_monitor  -p ${partition} -D  ${df} -M 4 -F 3 -N 0 >> ${file} #6
im_monitor  -p ${partition} -D  ${df} -M 4 -F 3 -N 1 >> ${file} #7
im_monitor  -p ${partition} -D  ${df} -M 4 -F 4 -N 0 >> ${file} #8
im_monitor  -p ${partition} -D  ${df} -M 4 -F 4 -N 1 >> ${file} #9
im_monitor  -p ${partition} -D  ${df} -M 4 -F 5 -N 0 >> ${file} #10
im_monitor  -p ${partition} -D  ${df} -M 4 -F 5 -N 1 >> ${file} #11
im_monitor  -p ${partition} -D  ${df} -M 4 -F 6 -N 0 >> ${file} #12
im_monitor  -p ${partition} -D  ${df} -M 4 -F 6 -N 1 >> ${file} #13
im_monitor  -p ${partition} -D  ${df} -M 4 -F 7 -N 0 >> ${file} #14
im_monitor  -p ${partition} -D  ${df} -M 4 -F 7 -N 1 >> ${file} #15
echo "      done"
echo -n "All spy buffers (IM+DF) (~40 sec + 0~120 sec(depend on EMon interval 120 sec...))"

echo "">> ${file}
echo "">> ${file}

im_monitor -p ${partition} -D ${df} -M 3 -R ${shelf} -l ${slot} -A 1 -n ${MonitoringOutput} 2>/dev/null >> ${file}

echo "      done"

# This checks that the spy buffer is at least 10,000 lines long
# If it isn't then it's assumed that the IM+DF spys weren't read, and complains
file_length=$(wc $file | awk '{print $1}')
if [ $file_length -lt 10000 ]
then
	echo "ERROR! IM+DF Spy Buffers not saved!"
	echo "This probably means you have an atypical RCD name"
	echo "Please rerun, pass the RCD name as an argument:"
	echo ">bash SaveAllSpyISEMon_IMDF.sh [IM-DF-AUX-AMB] [3] [3] [DF22] [FTK-RCD-DF-Slice2]"

elif [ $file_length -gt 200000 ]
then
	echo "File has extraneous words. Stripping redunant words."
	echo "This is a problem only for FTK-02-00-10"
	if [ $TESTBED = true ]
	then
		python /afs/cern.ch/user/r/rmina/public/FTK/strip_redundant_IM_spy.py ${file}
	else
		python /det/ftk/tools/strip_redundant_IM_spy.py ${file}
	fi
		echo "done."
	echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
	echo "H All IM spys were saved to " ${file}
	echo "H If you get error mesage, please try again.           H"
	echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"

else
	echo "All 16*4k IM input, 16*4k IM output, 16*1k DF input, 36 DF output SPYs was saved."
	echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
	echo "H All IM spys were saved to " ${file}
	echo "H If you get error mesage, please try again.           H"
	echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"

fi
