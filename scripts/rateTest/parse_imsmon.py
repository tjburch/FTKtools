# Parse IM SMON 
'''
Parses IM SMON outputs produced with dump_imsmon.py to pick out only average data rate
'''

import re, sys, glob

# DFs to run over
dfs = ["DF22"]
#dfs = ["DF-1-03", "DF-1-04","DF-1-05","DF-1-06","DF-1-07","DF-1-08" ,"DF-1-09","DF-1-10",
       #"DF-3-03", "DF-3-04","DF-3-05","DF-3-06","DF-3-07","DF-3-08" ,"DF-3-09","DF-3-10",
       #"DF-4-03", "DF-4-04","DF-4-05","DF-4-06","DF-4-07","DF-4-08" ,"DF-4-09","DF-4-10"]
#      ]
for df in dfs:
  for fpga in range(8): # 8 FPGAs 
   for channel in range(2): # 2 channels per FPGA
	# Find files corresponding to cycles for that DF, FPGA, channel
        files = glob.glob(""+df+"_F"+str(fpga)+"_N"+str(channel)+
                 "/"+df+"_F"+str(fpga)+"_N"+str(channel)+"_C*")
        rates = [] # this is where the averages ratesfor each im_monitor call g
        for my_file in files: # for each file
            with open(my_file,'r') as f: # open file
                for line in f: # search through each line
                     # The average rate will match this RegEx 
                     rate = re.search(r'Recent\|\s+\d+\|\s+\d+\|\s+\d+\|\s+(\d+)',line)
                     if rate: # If we get a match
		        rates.append(int(rate.group(1))) # Add it to our rates list
	if len(rates)!=0: # If we have an IMSMON file
        	print(df+"_F"+str(fpga)+"_N"+str(channel)
                      +" Average rate:     "+str((sum(rates))/len(rates)))
	else:
		print(df+"_F"+str(fpga)+"_N"+str(channel)+" Not monitored")
        #print(rates)
  print("")
