# enable xoff from DOWNSTRAM (e.g. df)
# test_im_bt -D DF1D -M 8 -F 0 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 0 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 1 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 1 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 2 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 2 -N 1 -V 1
# test_im_bt -D DF1D -M 8 -F 3 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 3 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 4 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 4 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 5 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 5 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 6 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 6 -N 1 -V 0
# test_im_bt -D DF1D -M 8 -F 7 -N 0 -V 0
# test_im_bt -D DF1D -M 8 -F 7 -N 1 -V 0


# enable SLINK 0; slink, 1: pseudo
test_im_bt -D DF22 -M 9 -F 0 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 0 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 1 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 1 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 2 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 2 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 3 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 3 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 4 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 4 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 5 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 5 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 6 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 6 -N 1 -V 0
test_im_bt -D DF22 -M 9 -F 7 -N 0 -V 0
test_im_bt -D DF22 -M 9 -F 7 -N 1 -V 0
