#test_df -M 1 -D DF1D -F ../config/configuration_initial_tower.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1 -F /tbed/user/kkrizka/DataFormatter/configuration_b0_f2c1.txt -X /tbed/user/kkrizka/DataFormatter/transceiver_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_testvector_20150721_NoIntLink.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_ch5.txt -X ../config/transceiver_configuration.txt

###test_df -M 1 -D DF1D -F ../config/configuration_real_NoSend.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_newLUT_20150828.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_real_LUT_NoSend_20150821.txt -X ../config/transceiver_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_ch0_NoSend_realLUT.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_ch0_realLUT_mod.txt -X ../config/transceiver_configuration.txt

#test_df -M 1 -D DF1D -F /tbed/user/kkrizka/DF-16ch/configuration_testvector_20150721_NoIntLink.txt -X /tbed/user/kkrizka/DF-16ch/transceiver_configuration.txt

#test_df -M 1 -D DF1D -F ../config/configuration_ch01_testvector.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_chCF.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_ch5.txt -X ../config/transceiver_configuration.txt
#test_df -M 1 -D DF1D -F ../config/configuration_fmc2.txt -X ../config/USA15_transceier_configuration_orig.txt

#test_df -M 1 -D DF1D -F ../config/configuration_for_16ch_test.txt -X ../config/USA15_transceier_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_for_16ch_2014.txt -X ../config/USA15_transceier_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_for_16ch_test_noSend.txt -X ../config/transceiver_configuration.txt

#test_df -M 1 -D DF22 -F ../config/configuration_DaveCheck_20151029.txt -X ../config/transceiver_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_newDelay_150MHz_NoSend_20151112.txt -X ../config/transceiver_configuration_orig.txt
#test_df -M 1 -D DF1D -F ../config/configuration_ForPbRun_onlysct_IntLink_DF2_125MHz_FOR_TEST_20151125.txt -X ../config/transceiver_configuration_orig.txt

test_df -M 1 -D DF22 -F ../config/configuration_DF31_20160510.txt -X ../config/transceiver_configuration_orig.txt
#test_df -M 1 -D DF22 -F ../config/config_b16_1df_32towers.txt -X  ../config/USA15_transceier_configuration_1df.txt

#test_df -M 1 -D DF22 -F /afs/cern.ch/work/t/tomoya/public/test_20160605/config/config_32towmodlist_mc15_b26_NEWRODS.txt -X /afs/cern.ch/work/t/tomoya/public/test_20160605/config/USA15_transceier_configuration_b0_1df.txt

# auto delay setting
#test_df -D DF22 -M 30
